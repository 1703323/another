'use strict';
const express = require('express');
const app = express();
// Add websocket support
const server = require('http').createServer(app);

const HOST = '127.0.0.1';
const PORT = 8000;

var points = 0;

app.use('/', express.static(__dirname + '/client'));

server.listen(PORT, function() {
  console.log('server up @ http://' + HOST + ':' + PORT);
});
